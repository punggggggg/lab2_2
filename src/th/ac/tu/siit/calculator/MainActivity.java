package th.ac.tu.siit.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button n1 = (Button) findViewById(R.id.num1);
		n1.setOnClickListener(this);

		Button n2 = (Button) findViewById(R.id.num2);
		n2.setOnClickListener(this);

		Button n3 = (Button) findViewById(R.id.num3);
		n3.setOnClickListener(this);

		Button n4 = (Button) findViewById(R.id.num4);
		n4.setOnClickListener(this);

		Button n5 = (Button) findViewById(R.id.num5);
		n5.setOnClickListener(this);

		Button n6 = (Button) findViewById(R.id.num6);
		n6.setOnClickListener(this);

		Button n7 = (Button) findViewById(R.id.num7);
		n7.setOnClickListener(this);

		Button n8 = (Button) findViewById(R.id.num8);
		n8.setOnClickListener(this);

		Button n9 = (Button) findViewById(R.id.num9);
		n9.setOnClickListener(this);

		Button n0 = (Button) findViewById(R.id.num0);
		n0.setOnClickListener(this);

		Button add = (Button) findViewById(R.id.add);
		add.setOnClickListener(this);

		Button bs = (Button) findViewById(R.id.bs);
		bs.setOnClickListener(this);
		
		Button sub = (Button) findViewById(R.id.sub);
		sub.setOnClickListener(this);
		
		Button ac = (Button) findViewById(R.id.ac);
		ac.setOnClickListener(this);
		
		Button mul = (Button) findViewById(R.id.mul);
		mul.setOnClickListener(this);
		
		Button div = (Button) findViewById(R.id.div);
		div.setOnClickListener(this);
		
		Button equ = (Button) findViewById(R.id.equ);
		equ.setOnClickListener(this);
		
		Button dot = (Button) findViewById(R.id.dot);
		dot.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	int last_sign = 0;
	double last_number = 0;
	String output = "0";
	
	@Override
	public void onClick(View v) {

		TextView outputTV = (TextView) findViewById(R.id.output);
		int id = v.getId();
		
		switch (id) {
		
		case R.id.num1:
		case R.id.num2:
		case R.id.num3:
		case R.id.num4:
		case R.id.num5:
		case R.id.num6:
		case R.id.num7:
		case R.id.num8:
		case R.id.num9:
		case R.id.num0:
		case R.id.dot:

			if (output.equals("0")) {
				output = "";
			}
			
			if (id == R.id.dot)
			{
				if(!output.contains("."))
					output += ((Button) v).getText().toString();
			}
			else
			{
				output += ((Button) v).getText().toString();
			}
			outputTV.setText(output);
			break;

		case R.id.bs:
			output = output.substring(0, output.length() - 1);
			if (output.equals(""))
				output = "0";
			outputTV.setText(output);
			break;

		case R.id.add:

			if (last_sign == 0) {
				last_number = Double.parseDouble(outputTV.getText().toString());
				last_sign = 1;

				output = "0";
			} else {
				// ////// EQU ////////
				double current_number = Double.parseDouble(outputTV.getText().toString());
				if (last_sign == 1)
					last_number = last_number + current_number;
				if (last_sign == 2)
					last_number = last_number - current_number;
				if (last_sign == 3)
					last_number = last_number * current_number;
				if (last_sign == 4)
					last_number = last_number / current_number;

				outputTV.setText(String.valueOf(last_number));
				// ///// EQU /////////

				last_number = Double.parseDouble(outputTV.getText().toString());
				last_sign = 1;

				output = "0";
			}
			break;
			
		case R.id.sub:
			if (last_sign == 0) {
				last_number = Double.parseDouble(outputTV.getText().toString());
				last_sign = 2;

				output = "0";
			} else {
				// ////// EQU ////////
				double current_number = Double.parseDouble(outputTV.getText().toString());
				if (last_sign == 1)
					last_number = last_number + current_number;
				if (last_sign == 2)
					last_number = last_number - current_number;
				if (last_sign == 3)
					last_number = last_number * current_number;
				if (last_sign == 4)
					last_number = last_number / current_number;

				outputTV.setText(String.valueOf(last_number));
				// ///// EQU /////////

				last_number = Double.parseDouble(outputTV.getText().toString());
				last_sign = 2;

				output = "0";
			}
			break;
			
		case R.id.ac:
			last_sign =0;
			last_number = 0;
			output = "0";
			outputTV.setText(output);
			break;
			
			
		case R.id.mul:
			if (last_sign == 0) {
				last_number = Double.parseDouble(outputTV.getText().toString());
				last_sign = 3;

				output = "0";
			} else {
				// ////// EQU ////////
				double current_number = Double.parseDouble(outputTV.getText().toString());
				if (last_sign == 1)
					last_number = last_number + current_number;
				if (last_sign == 2)
					last_number = last_number - current_number;
				if (last_sign == 3)
					last_number = last_number * current_number;
				if (last_sign == 4)
					last_number = last_number / current_number;

				outputTV.setText(String.valueOf(last_number));
				// ///// EQU /////////

				last_number = Double.parseDouble(outputTV.getText().toString());
				last_sign = 3;

				output = "0";
			}
			break;
			
		case R.id.div:
			if (last_sign == 0) {
				last_number = Double.parseDouble(outputTV.getText().toString());
				last_sign = 4;

				output = "0";
			} else {
				// ////// EQU ////////
				double current_number = Double.parseDouble(outputTV.getText().toString());
				if (last_sign == 1)
					last_number = last_number + current_number;
				if (last_sign == 2)
					last_number = last_number - current_number;
				if (last_sign == 3)
					last_number = last_number * current_number;
				if (last_sign == 4)
					last_number = last_number / current_number;

				outputTV.setText(String.valueOf(last_number));
				// ///// EQU /////////

				last_number = Double.parseDouble(outputTV.getText().toString());
				last_sign = 4;

				output = "0";
			}
			break;
			
		case R.id.equ:
			double current_number = Double.parseDouble(outputTV.getText().toString());
			if (last_sign == 1)
				last_number = last_number + current_number;
			if (last_sign == 2)
				last_number = last_number - current_number;
			if (last_sign == 3)
				last_number = last_number * current_number;
			if (last_sign == 4)
				last_number = last_number / current_number;

			outputTV.setText(String.valueOf(last_number));
			
			break;
			

		}

		

	}

}
